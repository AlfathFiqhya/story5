from django.urls import include, path
from .views import index, savename

app_name = 'homepage'

urlpatterns = [
    path('',index, name='index'),
    path('savename',savename)

]