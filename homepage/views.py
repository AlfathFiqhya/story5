from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form

def index(request):
    return render(request, 'index.html')

def savename(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
# Create your views here.
