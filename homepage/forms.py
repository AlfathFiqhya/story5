from django import forms
from .models import Person

class Input_Form(forms.ModelForm):
	class Meta:
		model = Person
		fields = ['display_name']
	error_messages = {
		'required' : 'Please Type'
	}
	input_attrs = {
		'type' : 'text',
		'placeholder' : 'Nama Kamu'
	}
	display_name = forms.CharField(label='', required=False, 
max_length=27, widget=forms.TextInput(attrs=input_attrs))
